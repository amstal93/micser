const express = require('express');
const Sequelize = require('sequelize');
const Redis = require('redis');
const getId = require('docker-container-id');

const app = express();
const config = {
    app: {
		port: process.env.APP_PORT || 3000,
		host: process.env.APP_HOST || '0.0.0.0'
	},
    db: {
        database: process.env.DB_DATABASE || 'test',
        username: process.env.DB_USERNAME || 'user',
        password: process.env.DB_PASSWORD || 'pass',
        connection: process.env.DB_CONNECTION || 'mysql',
        host: process.env.DB_HOST || 'localhost',
        port: process.env.DB_PORT || 3306
    },
    redis: {
        host: process.env.REDIS_HOST || '127.0.0.1',
        port: process.env.REDIS_PORT || 6379,
        password: process.env.REDIS_PASSWORD || null,
        db: process.env.REDIS_DB || 0
    }
}
const sequelize = new Sequelize(config.db.database, config.db.username, config.db.password, {
    dialect: config.db.connection,
    host: config.db.host,
    port: config.db.port,
    operatorsAliases: false
});
const client = Redis.createClient(config.redis.port, config.redis.host)
client.on('connect', () => {
	console.log(`[Redis] Connected to redis://${config.redis.host}:${config.redis.port}/${config.redis.db}`)
}).on('ready', () => {
	console.log('[Redis] Connection Ready')
}).on('reconnecting', () => {
	console.log('[Redis] Connection Reestablished')
}).on('warning', (msg) => {
	console.log('[Redis] Connection Waring')
	console.log(msg)
}).on('error', (err) => {
	console.log('[Redis] Connection Error')
	throw err
});

app.get('/', async (req, res) => {
    try {
        await sequelize.authenticate();
        console.log('Connection has been established successfully.');
        let container_id = await getId();
        res.status(200).send(`[${container_id}] Connection has been established successfully.`);
    } catch(err) {
        console.log(err.stack);
        res.status(500).send(`${err.stack}`);
    }
});

const server = app.listen(config.app.port, config.app.host);
server.on('listening', () => {
    const { address, port } = server.address();
	console.log(`Server listening at http://${address}:${port}`);
});
server.on('error', err => {
    if (err.syscall !== 'listen') {
        throw err;
    }

    switch (err.code) {
        case 'EACCES':
            console.error(`Port ${config.app.port} requires elevated privileges`);
            server.close();
            break;
        case 'EADDRINUSE':
            console.error(`Port ${config.app.port} is already in use`);
            server.close();
            break;
        default:
            throw err;
    }
});